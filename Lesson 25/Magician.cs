﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Magician : IEntity
    {
        public string Name { get; private set; }
        public string Birthtown { get; private set; }
        public string FavoriteSpell { get; private set; }


        public Magician (string name, string birthtown, string favoritespell)
        {
            this.Name = name;
            this.Birthtown = birthtown;
            this.FavoriteSpell = favoritespell;
        }
        public string this[string val]
        {
            get
            {
                if ("Name".Equals(val, StringComparison.OrdinalIgnoreCase))
                {
                    return this.Name;
                }
                else if (val.ToLower().Equals("Birthtown".ToLower()))
                {
                    return this.Birthtown;
                }
                else if (val.ToLower().Equals("FavoriteSpell".ToLower()))
                {
                    return this.FavoriteSpell;
                }
                else
                {
                    return "Unknown";
                }
            }
            set
            {
                if (val.ToLower().Equals("Name".ToLower()))
                {
                    Name = val;
                }
                else if (val.ToLower().Equals("Birthtown".ToLower()))
                {
                    Birthtown = val;
                }
                else if (val.ToLower().Equals("FavoriteSpell".ToLower()))
                {
                    FavoriteSpell = val;
                }
            }
        }

        public override string ToString()
        {
            return $"Name: { Name}, Birthtown: {Birthtown}, Favorite Spell: {FavoriteSpell} ";
        }
    }
}
