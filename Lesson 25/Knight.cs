﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Knight : IEntity
    {
        public string Name { get; private set; }
        public string Birthtown { get; private set; }
        public string Title { get; private set; }

        public Knight (string name, string birthtown, string title)
        {
            this.Name = name;
            this.Birthtown = birthtown;
            this.Title = title;
        }
        public string this [string val]
        {
            get
            {
                if (val.Equals("Name", StringComparison.OrdinalIgnoreCase))
                {
                    return this.Name;
                }
                else if (val.ToLower().Equals("Birthtown".ToLower()))
                {
                    return this.Birthtown;
                }
                else if (val.ToLower().Equals("Title".ToLower())) 
                {                    
                        return this.Title;                   
                }
                else
                {
                    return "Unknown";
                }
            }
            set
            {
                if (val.ToLower().Equals("Name".ToLower()))
                {
                    Name = val;
                }
                else if (val.ToLower().Equals("Birthtown".ToLower()))
                {
                    Birthtown = val;
                }
                else if (val.ToLower().Equals("Title".ToLower()))
                {
                    Title = val;
                }
            }
        }

        public override string ToString()
        {
            return $"Name: { Name}, Birthtown: {Birthtown}, Title: {Title} " ;
        }
    }
}
