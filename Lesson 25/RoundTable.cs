﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class RoundTable <T>: IEnumerable<T> where T : IEntity
    {
        private List<T> entities = new List<T>();

        public void Add(T entity)
        {
            entities.Add(entity);
        }
        public void RemoveAt(int index)
        {
            if (index >= 0 && index > entities.Count)
            {
                int indexInList = (index - entities.Count) - 1;
                entities.RemoveAt(indexInList);
            }
            else
                entities.RemoveAt(index);
        }

        public void Clear()
        {
            entities.Clear();
        }

        public void InsertAt(int index, T entity)
        {
            if (index >= 0 && index > entities.Count)
            {
                int indexInList = (index - entities.Count) - 1;
                entities.Insert(indexInList, entity);
            }
            else
                entities[index] = entity;
        }

        public void Sort ()
        {
            entities.Sort();
        }

        public List<T> GetRounded(int number)
        {
            List<T> result = new List<T> ();
            int whole = number/entities.Count;
            int remainder = number % entities.Count;

            if (number >= 0 && number > entities.Count)
            {
                for (int i = 0; i < whole; i++)
                {
                    for (int j = 0; j < entities.Count; j++)
                    {
                        result.Add(entities[j]);
                    }
                   
                }
                for (int k = 0; k < remainder; k++)
                {
                    result.Add(entities[k]);
                }
                return result;
            }
            else
            {
                for (int i = 0; i < number; i++)
                {
                    result.Add(entities[i]);
                }
                return result;
            }
        }

        public T this[int index]
        {
            get
            {
                T result = entities[index];
                return result;
            }            
        }

        public T this[string name]
        {
            get
            {            
                T result = entities.FirstOrDefault(e => e.Name == name);   
                return result;
                //return default(T);
            }
        }

        public override string ToString()
        {
            string result = null;
            foreach (var item in entities)
            {
                result += ", " +item.ToString();
            }
            return result;
        }

        public List<T> GetAllList()
        {
            List<T> result = new List<T>();
            //entities.CopyTo(result.ToArray());
            foreach (T item in entities)
            {
                result.Add(item);
            }
            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.entities.GetEnumerator();
        }

        public string ListToString(List<T> list)
        {
            string result = string.Empty;

            foreach (var item in list)
            {
                if (item.GetType() == typeof(Magician))
                {
                    Magician mag = (Magician)Convert.ChangeType(item, typeof(Magician));
                    result += mag.Name + Environment.NewLine;
                }
                else
                {
                    Knight knight = (Knight)Convert.ChangeType(item, typeof(Knight));
                    result += knight.Name + Environment.NewLine;
                }
            }

            return result;
        }
        


    }
}
