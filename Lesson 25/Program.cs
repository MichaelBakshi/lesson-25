﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_25
{
    class Program
    {
        static void Main(string[] args)
        {

            RoundTable<Magician> magicians = new RoundTable<Magician>();
            RoundTable<Knight> knights = new RoundTable<Knight>();

            Magician magician1 = new Magician("Edward", "Bristol", "Attraction spell");
            Magician magician2 = new Magician("Tunde", "Zambia", "Memory erasing spell");
            Magician magician3 = new Magician("Ivan", "Serbia", "Blinding spell");
            Magician magician4 = new Magician("Matthew", "Liverpool", "Dissapearance spell");

            magicians.Add(magician1);
            magicians.Add(magician2);
            magicians.Add(magician3);

            Knight knight1 = new Knight("Eric", "York", "Sir");
            Knight knight2 = new Knight("Henry", "Birmingham", "Earl");
            Knight knight3 = new Knight("William", "Leeds", "Bachelor");

            knights.Add(knight1);
            knights.Add(knight2);
            knights.Add(knight3);

            foreach (Knight item in knights)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            foreach (Magician item in magicians)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            knights.RemoveAt(1);
            foreach (Knight item in knights)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            magicians.InsertAt(5, magician4);
            foreach (Magician item in magicians)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            Console.WriteLine(magicians.ListToString(magicians.GetAllList()));

            Console.WriteLine(knights.ListToString(knights.GetRounded(5)));
        }
    }
}
