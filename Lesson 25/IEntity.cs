﻿namespace Lesson_25
{
    internal interface IEntity
    {
        string Name { get; }
    }
}